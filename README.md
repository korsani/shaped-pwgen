
# shaped-pwgen

Generate a shaped bunch of passwords

# Starting

## Pre-requisite

- a working ```bash(1)```
- ```bc(1)```
- ```openssl(1)```
- eventually ```haveged(8)```

## Installation

Copy ```spwgen``` somewhere accessible by your ```$PATH```.

And/or alias it by ```alias pwgen=spwgen```

# Running

It's as simple as
```
$ spwgen
vgec1eYJqimtgtQ1u20h2bfqpcsTVtaFGnKdkVLUtxCSodwUZjOMcMpZIXE4A0F8
8pXZ0Sbwp8fOZlAGplhWLuk1tQb6WBtQDLnR9QxK5hG1BVJaQRCZgr76LEm9OYO2
fEs05F0gDk5DM0WUUfJgtAYM6bOepTD65WIHwan8xXp0eZTvlYlQI3053YbdMmic
iBFdBRVq8ns0CGLbOAX6Tw0OEiuhSa0AJZRdNNf3wRr39s4KCbdhrMqZl7561MVJ
2wrttBvDoxRS79d0olmdC07IMisOxhZTNfBPsR3qHzIDK3EymD9ocuWv8DFC1tqG
3LoWnoo4jlERkhPi6HbnBwKPf3A7wJvQJQYwAbTIZ77eEZ8pHfHB48k41gyJ7Ab2
e9nb6UMlyDXc2mGtu19kIXvsaMdwSrnrq0D2RGMWpW3Ex9Apjozr9pxdefWNqRUr
zpgePll42V0IssLknaSNeBhwE9oaVW5yqkHn6Q6O3JL6LdQMqE16KQuXD39xjQrp
twdcY5Mh2buoRKPipRAtxjgcYDtDNXOItiXBXlCU6lN1Up6WPqgfa7mmO8WDlhMw
LNWSSf0qYMLybVMpGICB9PoNjfOREDdD6eZBaFuA7hkc3WtwNomN6ZKIkpeSgCvi
cWhm7h5XGaBvaijBVQEJqOPKFSdfzsFt4M3Xf0fZiuO5KTxgU1uGQuv9haVY7aQn
88Z46zy4gYKmrkDjOB6dvNgPlTrov2mwFGE0iDW1fgXAFueolLUiRJ6KdJVY41GV
6HOuF4DuGZubqWZYuuP8ztbMA3OowbWjIHO3YUYtkKeqv76HmKbTaVaIC8Du9hnq
6JJFzATTuypxTYbm42Q1Krw0gwwsNDpbLOtkdmrR3eoO7dnKNUgEVquyxmFaIcQq
DzfouGqG4YcIQ62bUnB9Fokugjqf8VTyIg3RhNDnO7EZp7jDLqNmltm5ID1sdC6X
FhwG9FSk5fQlrucADKbWyXKbFC4SGwbQpKPnGn6pHgCjEeHBwFG3WiDB8WTcS6Xp
XOuI9jOf2VTqRaOAv1Z05QCGBxxi1vzSxF4nsb9VrFNwbSSUXFyHEoNkNKhrcSFr
FWmP4ozkotR9JsnDy0vIQgdTXXklE1qPELxXyNzZxF52XVKgaGpk9SYRpP4JoJxo
GX3yzNylNqBh8INdcaErvzCSB8TJn5ygsMNpN4DaH95IY35u16db7SIveiq5J8oC
3b4K2s9IL1M3t1DQ9AxngOkmRGfY6oanrKGgtl8DC5SpQrWJutcRkcM3NePIbswi
iwS2WdMk9YBKYLrEkt96dsgt1lRtRUZ0RH2XX04AXA8cI8KncKL21usLFcTv1sD6
kVFYtHj1o1OniB0Zu80hPncZTwu5yowmBEh6HfeMUPY12aw6k6zVA7pdIFasdY89
WIfYpOegz1GiHaij4nySgwcvhEg4PQUclxnTy9eZ43lcVtqE8Z71tG0ozcD066Wz
KeZYKtcgfkpxI7odxUaqQKf1TPmKYftZpbFYUPpf7lU0WeOUvtQoiRPsdZxnHggg
oVZ5OYlj04AI3snZLSUpjSOgrVGmU65Mb8Z1fhgqsCGP0RfMd7LvXshEdOS7Q31V
lrCykFkEXIFzV9gj0rJA6EytWXB7ainroWpSicWZdUwYhix9ZRDnVtvmqxzqAFxI
bq2tME2TPu7BrCydCb3Xu3sEU7K8R22S8oV79TtILOyGjnAfx5wSoUiDR1ReFUJc
02quKJ8Pfq8vGKXOQemOA2y7dEd39UquLnZTMm9kwP4EJarsWGIidyIca7hT0bDB
GwNZ2o8lB5n9nDayoYjafvccXwrc7cptKXo5z6s5w3qAOdrH8XxnzVfYWp2vFA99
A4AoIeSeZpbsOwantW7TV9q74yqKeVMKFVsHaRMzxKY8Hao9otoLhnAZWQs7izl1
USxfnqcXZw5U33PAg2dUeaLsDPvfvmCZ8Xogob177Av3qbYYgl8GyKCK7K0Cu4L5
AWqiomKzZCNa3382M6uwiPu0WFoRjWfl1xMqFhfZvycUNZvULRYyzNNDYvkWpsAA

$ spwgen -a circle -l -s
  0   0 |                                  
157  24 |                      E)9UFz3d(eZmJ$+)'NbEY"$s
209  32 |                  WC_%SZ:d;M1s[Jw{V]-seF(mZ9+x[f_i
249  38 |               &W6."9(6C|&!B%Cfa=sgFGuWbDUQ,]!K2(vGaz
275  42 |             T]l4'g=0ZDn4[\O3yi/'sTS5F)/wP&>!bQd2R30w:3
308  47 |          xM~DZR\al_a+w/+Bc]dA*$Bb3m7Z/BMtE)Ja@ohblU4QX+?
327  50 |         ?`eZkqzF+uY4HFZv\)G9qVe'sOqw'zd$;AtozqQt&,s@^Sa<M^
347  53 |       X1;Q5K#%UwfFGlnR!ar]7^`pj^\3T:wd(x.!{p/O;+-E[^.cHbt'm
360  55 |      L9.^,r.1%vWpdc/Jrda6{dgk9M[~KaXyV~Ygpc)hsLgP^Nq,Qwm_B;s
380  58 |     (xwuK3-;qJZW%elMI=jG{dlJh<Mw$b5m_b6Cz2D1jl6!9!jOGb{wuIi>nH
393  60 |    zBS55]+!G=h"EHZ.j*/t56)Klv8tdm,&hMUOF~yzrg#*P4)utPS\JL(yIXlZ
399  61 |   cc23<&wk{?BJ8e!)8E{?v#ra}2nanCDl[1&`K:.NOOfUf5JvcYpg2.qa[S-AI
399  61 |   ApI$QwHig;v9'x18a@pWTv^2Bi<QVzM_/Uq{08s=V5$RLh\X_,OW^ZmJFj&iH
412  63 |  1qKs@NWEfT1~yF4f\(nW@.cq"&9}>/H[[#u(:H#_W/SYE<b(d|?dC8#X>_F%494
412  63 |  n8q(7jiTvKjbI;fe@&0>C0kU`06?8@;:NL71$e8Sh6c^-$\83(PEHKo'J/8[=-O
419  64 |  uu/u=R~$:mG%u0jXB4uRG~w#\o:1/}{Fs^#zay:G^?55Gy]DRA%(zqrY/oC#%r%]
419  64 |  M/eT;Y.dp_m%jO%*<hp}8?w8mdlv5W<&UHAY,uE{2-iU)G.H-E8y7_bnY:|3#uNh
419  64 |  4~)#.@K#IfrN&Fd]`eA'&NU>1ul]H=mG(]@<r=EU]f(`R`GvFpi2]Em<*>b$vHj}
412  63 |  (6!:JXAF)Y2M$3YSkp}E.<Vd~7Q[$)AY:aZds1r}-(&j7?r.@w'RwelZ)zhiI/7
412  63 |  z+5,nVaedRFt$(Qh44kc>U\B1/zrKa1dM'@D)FE<"QWDnZ$"&3NYRdkj5Eh";My
399  61 |   Y}bRZ{hHzgn<2k?_~4lLL([CscG>T<$-jZEAm%*J,~"MI(ukfr:_/hLF\[r2/
399  61 |   e"Nv%U"s6@n:>#1,n-Dq)B7sh7-.X:NKkdwnvd/@Xi);!"6o}&*3gP{tjtg\_
393  60 |    NCInh&D`:,<iGTf.{npDO(gj-7*;]C5~%lK]EfC\~iyh}r0wJf_)2`EbZOML
380  58 |     5VJ7&,1uj}9{.'94]nx,5%mJfA"'}BM+~-OOmP{q*@1y&-#nyu)OL'|7v>
360  55 |      q%:Xr4sbe~(leH&&/lhQzzw[vh.,x}M3wvfioag[t;h<9YiBB]b2yL]
347  53 |       NTUr|mHW-;`SB7tE'`!a9VF,T/VRHYYQ5kH@YD%C>Zh=p5Q+qlE&&
327  50 |         *EhMyhM8/Jw1suX[Wmv+Bp6f"5\kAt{LbC0zaW?aP3=V+1+m#D
308  47 |          l7cZ@a>Q<Pv-sx%Z0*4ya(dbXUC<q:]=*N;xI!`sr?/mtp7
275  42 |             b6pq0]4R%T`|?"n=B\b.}itO{.%BtPnJ*|fE6uaih;
249  38 |               vOUe+oxFLgwg8IRn]4>=Sgl!"3t4qqi{_he%DS
209  32 |                  U4dc%kXFu/F/pv[~P=t5Jjk,As$$FuHq
157  24 |                      l1)ov(]:aN?e*1oB:xQ>oBfm
```

The former generate a square of 64 char length passwords. As they contain no special chars, they should be easy to copy/paste.

The latter generate max 64 char long password, colored circle shaped, with special chars. Each line is prepended by the entropy and password length, and colored.

## RNG source

It uses ```openssl(1)``` by default, but can also use ```haveged(8)``` if available, ```/dev/random``` or an external command.

	$ spwgen -e haveged
	$ spwgen -e random
	$ spwgen -e exec -o exec=/path/to/cmd

## Password strength, length and number

```spwgen(1)``` compute max 64 char length passwords without special chars, that should be strong enough for years, according to [this](https://generatepasswords.org/how-to-calculate-entropy/).

You can specify length, number, and more (or less) secure passwords:

	$ spwgen 128    # 128 char long
	$ spwgen -n 128 # same, but password contain only numbers 0 to 9
	$ spwgen -s     # give me some special chars
	$ spwgen -s -12 # give me only 12, but secured
	$ spwgen -f     # a full screen height of passwords
	$ spwgen -k     # compute password size so that it's short BUT secure enough
	$ spwgen -xk    # password will secure and hexa only

Specify ```-c``` if you want to be sure that password contains at lease one char of each class:

	$ spwgen -kc
	$ spwgen -xc 4

Password is considered weak if entropy is < 40 nats, and strong if entropy > 70 nats. This may (will) vary...

### Stats

A 10 char length (60 nats entropy) password generated with ```spwgen -1 10``` take, with ```hashcat(1)``` 6.2.5 (```hashcat -O -a 3 -m <type> --custom-charset1='?l?u?d' <file> '?1?1?1?1?1?1?1?1?1?1'```):

| device                       | hashcat | OS             |  hash     | time
|:----------------------------:|:--------|:---------------|:----------|:------
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | md5       | 86y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | sha1      | 200y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | sha256    | 500y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | whirlpool | next big bang

A 9 char length (59 bit entropy) password generated with ```spwgen -1 -s 9``` take, with ```hashcat(1)``` 6.2.5 (```hashcat -O -a 3 -m <type> --custom-charset1='?l?u?d?s' <file> '?1?1?1?1?1?1?1?1?1'```):

| device                       | hashcat | OS             |  hash     | time
|:----------------------------:|:--------|:---------------|:----------|:------
| RPi4                         | 6.1.1   | Linux Raspbian | md5       | 1133y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | md5       | 40y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | sha1      | 160y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | sha2      | 390y
|Intel Core i5 i5-8265U 1.6GHz | 6.2.5   | Windows        | whirlpool | next big bang

## Shapes

```spwgen(1)``` display your passwords in square, circle, cross, dice or triangle. Default is square.
	
	$ spwgen -a circle 42 -s    # a circle shaped of 42 char long secured passwords
	$ spwgen -a dice -x 8       # show a 8 char long hexadecimal password in a rolling dice manner

If an image file is provided by ```-i```, ```spwgen(1)``` will shaped passwords accordingly (provided ImageMagick is installed).


```
$ spwgen -i img/cthulhu.png

                             jDLM3
                     tCgrKc83gnExQuL9EinEfs
                 112cVHvp1YZQ      4Kkbv6dTyKVw
              YHQgaVsZz3                21TmJHz0az
            7jTf L   3M                   M   1 9Kzy
          e7T9KVe8   b                    T   Nif9SUzb
        0uM    rJ   O                      6   nY    1r6
       Jd  gpN    Gl                        R3    V0p  3A
      uK AqO      5C                        yW      7Lk pc
     QyKDf   aYC  UTq                      uDb  Voz   LgEE2
    JYit   tbpDS  6GREp                  V7qeB  KvN5v   FwIO
    c1J   PUXY3   A  NW                  O0  0   oAFUC   22b
   2UTz  26igQ   Z                            D   0kfJs  1PrL
   5ccK  79ME   9o       B2          14        D   TBj6   3zu
   QRPf   RM  UPbz        uep      7er    F   7UmX  tM0  G2Dc
   iDMvh     2whXiKvQc       j    b       LWEtAxy0i     vgKSa
   2eDqbE                      9a                      fAAfA1
   BSJ4V   UK2SDC  c9L         tE         YVbLgJBM1mJ   B0pG3
    o24Q   IIB    v4q                      fWP     Oj   tN40
    cPMR   8n AFx     QVDg            BxDQ     2l6 AJ   walM
     J67D  PVSjR     5k2z       G      PSWnI    dc4mp  o2BP
      z76i   xO   O  eB    aq  wh  YJ    KE      x3   ocT1
       lH3Ou            TVm83  Yg   egp6            UbkRB
        gImfc6    p  3eTH      I5      Jugp       Pezfjg
          kffE   ms       4    bk    D       Cm   TbiT
            oHT        hdF   x  a b   ILs        CdV
              OAJ3etN       zQ KU FM       IsnVDM5
                 FDzB2Ao2dOPe DK6O enRbkwHsH2cO
                     xtWNcpqIZuBLgnM696tQJR
                              3MjBa

```

Or a numbered fish:

```
$ spwgen -n -i img/fish.png

                             5096
                            48744598
                            87969410744
                            9356037934284
                            5798195494787287229
                        184378991549617033181179911
  30067              430410065033337158125234755213213
   8528153         50670154436929481053261037469926096411
   3406350978    690978461970195375709842853332522805146250
    2021155108503145058778347760770888085250312   6867778739
     2971259517364091233988956494487898536698       7112779055
     21862048748658614700013331830464789306818      687555468
    80956560302003370858445776203892738437128834 99105146979
   345730147     36349607988865704972677982673929028783352
   2078514         2218974202759627603771099913088043557
  16559              112864473085493247598287252513780
                         91634089113894864285800166
                            7178547719825591788
                            6011852787381
                            8544309591
                            68879890
                             062

```

Give ```spwgen -li img/cthulhu.png``` a try ;)

If image file is ```-```, data will be take from stdin: every non blank char will be replaced by a random one (unless ```-o spray=<file>```)

```
$ figlet "Password" -p -o -W -f banner | spwgen -i-
 dme1qF
 K     u    Gu     BKLx    ZiPO   a    z   KLb9   MX63Q   nP0Y7
 2     Z   a  b   j       0       G    r  9    I  J    g  K    h
 fljO2S   t    p   y9fq    bxMx   U    d  v    y  q    D  c    q
 b        swyzjo       p       3  g qU X  3    P  mfSuG   2    p
 p        3    O  G    X  i    R  oP  yR  P    X  k   s   8    0
 J        l    1   Bkmk    haqv   A    b   yWNs   I    K  gjbmy
```

## Misc

	$ spwgen -q		# Do not prepend lines by password length
	$ spwgen -v		# Be verbose
	$ spwgen -d		# Be more verbose

## Examples

	$ spwgen -n 4                                                                     # Create 4-digit pin
	$ spwgen 10 | tr '[a-z]' '[A-Z]' | sed -e 's/\(....\)\(...\)\(...\)/\1-\2-\3/'    # Create neosurf codes
	$ spwgen -n 16 -1 | fold -w 4 | xargs                                             # Create paysafecard codes
	$ spwgen -n 9 | while read n ; do echo "0$((6+RANDOM/16384))$n" ; done            # Create french mobile phone numbers

# Discussions

## On password length

Entropy, in nats, is given by the formula $$ E = {log(S^L) \over log(2)} $$ or $$ E = {L*log(S) \over log(2)} $$ where S is the size of the pool and L the password length.

```spwgen``` has those pools :

- NUM: numbers (0-9)
- HEX: hexadecimal chars (a-f,0-0)
- ALNUM: alphanumeric chars (a-z, A-Z, 0-9)
- GRAPH: ALNUM plus specials chars

Size of pools are:

| Pool name | Size |
|:------|:-----|
|NUM    | 10   |
|HEX    | 16   |
|ALNUM  | 62   |
|GRAPH  | 94   |

Given the entropy formula, you can calculate the following password and entropy pool size:

|Entropy | NUM (10)	| HEX (16)	| ALNUM (62)	| GRAPH (94) |
|---|---|---|---|---
| 20 | 6  | 5  | 4  | 3
| 26 | 8  | 7  | 5  | 4
| 33 | 10  | 9  | 6  | 5
| 39 | 12  | 10  | 7  | 6
| 46 | 14  | 12  | 8  | 7
| 52 | 16  | 14  | 9  | 8
| 59 | 18  | 15  | 10  | 9
| 66 | 20  | 17  | 12  | 10
| 72 | 22  | 19  | 13  | 11
| 79 | 24  | 20  | 14  | 12
| 85 | 26  | 22  | 15  | 13
| 92 | 28  | 23  | 16  | 14
| 98 | 30  | 25  | 17  | 15
| 105 | 32  | 27  | 18  | 16
| 111 | 34  | 28  | 19  | 17
| 118 | 36  | 30  | 20  | 18
| 125 | 38  | 32  | 21  | 19
| 131 | 40  | 33  | 23  | 20
| 138 | 42  | 35  | 24  | 21
| 144 | 44  | 37  | 25  | 22
| 151 | 46  | 38  | 26  | 23
| 157 | 48  | 40  | 27  | 24
| 164 | 50  | 41  | 28  | 25
| 170 | 52  | 43  | 29  | 26
| 177 | 54  | 45  | 30  | 27
| 184 | 56  | 46  | 31  | 28
| 190 | 58  | 48  | 32  | 29
| 197 | 60  | 50  | 34  | 30
| 203 | 62  | 51  | 35  | 31
| 210 | 64  | 53  | 36  | 32
| 216 | 66  | 55  | 37  | 33
| 223 | 68  | 56  | 38  | 34
| 229 | 70  | 58  | 39  | 35
| 236 | 72  | 59  | 40  | 36
| 243 | 74  | 61  | 41  | 37
| 249 | 75  | 63  | 42  | 38
| 256 | 77  | 64  | 43  | 39
| 262 | 79  | 66  | 45  | 40
| 269 | 81  | 68  | 46  | 41
| 275 | 83  | 69  | 47  | 42
| 282 | 85  | 71  | 48  | 43
| 288 | 87  | 73  | 49  | 44
| 295 | 89  | 74  | 50  | 45
| 302 | 91  | 76  | 51  | 46
| 308 | 93  | 78  | 52  | 47
| 315 | 95  | 79  | 53  | 48
| 321 | 97  | 81  | 54  | 49
| 328 | 99  | 82  | 56  | 50
| 334 | 101  | 84  | 57  | 51
| 341 | 103  | 86  | 58  | 52
| 347 | 105  | 87  | 59  | 53
| 354 | 107  | 89  | 60  | 54
| 361 | 109  | 91  | 61  | 55
| 367 | 111  | 92  | 62  | 56
| 374 | 113  | 94  | 63  | 57
| 380 | 115  | 96  | 64  | 58
| 387 | 117  | 97  | 65  | 59
| 393 | 119  | 99  | 67  | 60
| 400 | 121  | 100  | 68  | 61
| 406 | 123  | 102  | 69  | 62
| 413 | 125  | 104  | 70  | 63
| 419 | 127  | 105  | 71  | 64

You can read that a 64 char length password of pool NUM (size: 10) is equivalent to a 32 char length password of pool GRAPH (size: 94) and has an entropy of 210 nats.

Here the formula to calculate the length L1 needed for a password from pool P1 of size S1 to have the same entropy as a password of length L2 from pool P2 of size S2:

  $$ L1 = {L2*log(S2) \over log(S1)} $$

This should give you an answer to questions like "My 38 char password has specials char that mess up my script, how long has to be a password without special chars?"

Answer is 42 (of course).

## On password entropy calculation

As said befor, the entropy of a string is given by the formula $$ E={L*log(S) \over log(2)} $$, where S is the size of the pool and L the password length.

Given an arbitrary string, what is the pool size used to generate that string? Say you have the string 'a'. Does it belong to the 'a-z' pool? 'a-zA-Z' one? or the 'a' one? The pool size are respectively 26, 52 and 1. And entropy 4.7, 5.7 and 0.

If you have no idea of the algorithm used to generate the string, you can't guess the pool used. So, I found questionnable the web sites or software that gives you the entropy of your string just by typing it...

```spwgen(1)``` offers you two ways of calculating password entropy: pessimistic and optimistic:

- **pessimistic** assumes each char belong to the smallest available pool: 'a' belong to a-f0-9, 'z' to a-zA-Z0-9, '0' to 0-9, '%' to the a-zA-Z0-9 and specials chars. It the sums the entropy of each chars. Entropy of 'az' is $$ $$ {log(16) \over log(2)}+{log(62) \over log(2)} = 10 $$
- **optimistic** assumes the pool used to generate the string is the biggest pool that contain at least one char of the string. 'a' belong to a-f0-9, 'z' to a-zA-Z0-9. Pool of 'z' is the biggest, it includes the pool of 'a', so entropy is $$ {2*log(62) \over log(2)}=12 $$

I actually have no idea wether this consideration makes sense... If you have ideas about it, feel free to contact me!

# Caveats

- Shapes rendering speed can be improved
- Especially when using ```-l```

# Made with

- Patience
- ```bash(1)```, ```bc(1)```, ```tr(1)```
- ```shellcheck(1)```
- [ImageMagick](https://imagemagick.org/script/index.php)
- Tested on GNU/Linux, Mac OSX, FreeBSD
